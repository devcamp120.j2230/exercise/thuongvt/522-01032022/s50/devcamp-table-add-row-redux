import { TASK_INPUT_CHANGE, TASK_ADD_CLICK } from "../constants/Task.Constants";

// nơi mô tả sự kiện liên quan tới task chứa các hàm return về mô tả
const inputChangeHandler = (inputValue)=>{
    return { // dạng ojb 
        type: TASK_INPUT_CHANGE,
        payload: inputValue
    }
}
 const addTaskClickAction = ()=>{
    return {
        type : TASK_ADD_CLICK
    }
 }
export {
    inputChangeHandler,
    addTaskClickAction
}