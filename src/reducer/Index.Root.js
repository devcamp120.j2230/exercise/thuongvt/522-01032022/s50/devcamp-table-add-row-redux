//Root reducer 
import { combineReducers } from "redux";
import taskReducer from "./Task.Reducer";

// Reducer là một oject
const rootReducer = combineReducers ({
        // improt task reducer qua indexRoot
        taskReducer: taskReducer
});

export default rootReducer;