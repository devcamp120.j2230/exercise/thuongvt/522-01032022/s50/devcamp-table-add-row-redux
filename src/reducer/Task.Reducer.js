import { TASK_INPUT_CHANGE,TASK_ADD_CLICK } from "../constants/Task.Constants"

const initalState = {
    inputString:"",
    taskLits:[]
}

const taskReducer = (state = initalState, action)=>{
    console.log(action)
    switch (action.type) {
        case TASK_INPUT_CHANGE:
            state.inputString = action.payload;
            break;
        case TASK_ADD_CLICK:
            if(state.inputString){
                state.taskLits.push({
                    taskName: state.inputString,
                    status:0
                })
                state.inputString = ""
            }
            break;
        default:
            break;
    }
        return {...state}

}

export default taskReducer