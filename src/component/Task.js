import { Button, Container, Grid, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { inputChangeHandler, addTaskClickAction } from "../actions/Task.Action";

function Task() {
    
    const dispatch = useDispatch()

    const {inputString,taskLits} = useSelector((reduxData)=>{
        console.log(reduxData)
        return reduxData.taskReducer
    })
    console.log(taskLits)

    // khai báo hành động
    const inputTaskChangeHandler = (event)=>{
        dispatch(inputChangeHandler(event.target.value))
    }
    const addTaskClickHandler = ()=>{
        dispatch(addTaskClickAction())
    }

    return (
        <Container>
            <Grid container mt={5} alignItems="center">
                <Grid item xs={12} md={2} ld={2} sm={12} textAlign="center">
                    <p>Nhập nội dung dòng</p>
                </Grid>
                <Grid item xs={12} md={6} ld={8} sm={12}>
                    <TextField label="Nội dung" variant="outlined" fullWidth value={inputString} onChange={inputTaskChangeHandler}/>
                </Grid>
                <Grid item xs={12} md={4} ld={2} sm={12} textAlign="center">
                    <Button variant="contained" onClick={addTaskClickHandler}>Thêm</Button>
                </Grid>
            </Grid>
            <Grid container mt={5} alignItems="center">
                <Grid item xs={12} md={12} ld={10} sm={12} >
                <TableContainer >
                    <TableHead>
                        <TableRow>
                            <TableCell align="center" style={{minWidth:"200px",backgroundColor:"gray"}}>
                                <h3>STT</h3>
                            </TableCell>
                            <TableCell align="center" style={{minWidth:"880px", backgroundColor:"gray"}}>
                                <h3>Nội Dung</h3>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                    {taskLits.map((element, index)=>{
                                return <TableRow>
                                    <TableCell align="center" style={{minWidth:"200px"}} key={index}>
                                        {index+1}
                                    </TableCell>
                                    <TableCell align="center" style={{minWidth:"200px"}} key={index}>
                                        {element.taskName}
                                    </TableCell>
                                </TableRow>
                            })}
                    </TableBody>
                </TableContainer>
                </Grid>
            </Grid>
        </Container>
    )
}

export default Task